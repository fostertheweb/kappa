//
//  AppDelegate.h
//  Kappa
//
//  Created by Jonathan Foster on 8/28/15.
//  Copyright © 2015 Jonathan Foster. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

