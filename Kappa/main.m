//
//  main.m
//  Kappa
//
//  Created by Jonathan Foster on 8/28/15.
//  Copyright © 2015 Jonathan Foster. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
