//
//  SplitViewController.m
//  Kappa
//
//  Created by Jonathan Foster on 9/1/15.
//  Copyright (c) 2015 Jonathan Foster. All rights reserved.
//

#import "SplitViewController.h"
#import "ChatViewController.h"
#import "PlayerViewController.h"
#import "TwitchKit.h"

@interface SplitViewController()

@property (weak, nonatomic) ChatViewController *chatViewController;
@property (weak, nonatomic) PlayerViewController *playerViewController;
@property (weak, nonatomic) NSView *rightView;
@property (weak, nonatomic) NSView *leftView;

@end

@implementation SplitViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.mainWindow = [[[NSApplication sharedApplication] windows] objectAtIndex:0];
    self.mainWindow.delegate = self;
    self.mainWindow.titleVisibility = NSWindowTitleHidden;
    
    // access view controllers
    self.playerViewController = [self.childViewControllers firstObject];
    self.chatViewController = [self.childViewControllers lastObject];
    
    self.rightView = [self.splitView.subviews lastObject];
    self.leftView = [self.splitView.subviews firstObject];
    
    // hide chat by default
    [self closeChat];
    
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (IBAction)searchWithSearchFieldContents:(id)sender {
    if ([[sender stringValue] length] == 0) {
        NSLog(@"no channel entered");
    } else {
        self.username = [sender stringValue];
        NSURL *streamURL = [TwitchKit getStreamURLForChannel:[sender stringValue]];
        [self.playerViewController startPlayerWithURL:streamURL];
        [self loadChat];
    }
}

- (void)toggleChatView:(id)sender {
    if (self.username == nil) {
        NSLog(@"no channel loaded");
        return;
    }
    
    if ([self.rightView isHidden] == YES) {
        [self openChat];
        [self loadChat];
    } else {
        [self closeChat];
    }
}

- (void)loadChat {
    NSURL *chatURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.twitch.tv/%@/chat", self.username]];
    
    if (![[self.chatViewController.chatWebView mainFrameURL] isEqualToString:[chatURL absoluteString]]) {
        NSURLRequest *chatRequest = [NSURLRequest requestWithURL:chatURL];
        [[self.chatViewController.chatWebView mainFrame] loadRequest:chatRequest];
    }
}

- (void)closeChat {
    [self.rightView setHidden:YES];
    CGRect viewFrame = self.splitView.frame;
    [self.leftView setFrame:NSMakeRect(0, 0, viewFrame.size.width, viewFrame.size.height)];
    [self.splitView display];
}

- (void)openChat {
    [self.rightView setHidden:NO];
    CGRect viewFrame = self.splitView.frame;
    [self.rightView setFrame:NSMakeRect(0, 0, 400, viewFrame.size.height)];
    [self.splitView display];
}

@end
