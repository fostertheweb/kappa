//
//  AppDelegate.m
//  Kappa
//
//  Created by Jonathan Foster on 8/28/15.
//  Copyright © 2015 Jonathan Foster. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
