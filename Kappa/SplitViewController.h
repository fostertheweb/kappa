//
//  ViewController.h
//  Kappa
//
//  Created by Jonathan Foster on 9/1/15.
//  Copyright (c) 2015 Jonathan Foster. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SplitViewController : NSSplitViewController <NSWindowDelegate>

@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSWindow *mainWindow;

- (IBAction)searchWithSearchFieldContents:(id)sender;
- (IBAction)toggleChatView:(id)sender;

@end

