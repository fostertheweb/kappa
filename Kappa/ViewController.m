//
//  ViewController.m
//  Kappa
//
//  Created by Jonathan Foster on 8/28/15.
//  Copyright © 2015 Jonathan Foster. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

@end
